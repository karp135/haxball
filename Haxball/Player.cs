﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;

namespace Haxball
{
    class Player
    {
        public static int playerCircleSize = 50;

        public ManualResetEvent manualResetEvent = new ManualResetEvent(false);

        public Point centerCoordinates = new Point();

        private List<Point> playersCoordinates = new List<Point>();

        public Point CenterCoordinates
        {
            get
            {
                return centerCoordinates;
            }
            set
            {
                centerCoordinates = value;
            }
        }

        public int CenterCoordinatesX
        {
            get
            {
                return CenterCoordinates.X;
            }
            set
            {
                CenterCoordinates = new Point(value, CenterCoordinates.Y);
            }
        }

        public int CenterCoordinatesY
        {
            get
            {
                return CenterCoordinates.Y;
            }
            set
            {
                CenterCoordinates = new Point(CenterCoordinates.X, value);
            }
        }

        // contents of movement algorithm below
        Point directionOfDestination;
        float x, y;
        float xIncrement, yIncrement;

        public void startMoving()
        {
            x = 0;
            y = 0;
            directionOfDestination = determineVectorToGo();
            determineMovementSettings();

            do
            {
                manualResetEvent.Reset();
                if (BallPhysics.isMoving())
                {
                    //x = 0;
                   // y = 0;
                    directionOfDestination = determineVectorToGo();
                    determineMovementSettings();
                }

                // below - move only if you're sure that you won't collide with nobody
                if (isMovementPossible())
                {
                    if (x > y)
                    {
                        moveY(directionOfDestination.Y);  //up or down
                        y += yIncrement;
                    }
                    else
                    {
                        moveX(directionOfDestination.X); //left or right
                        x += xIncrement;
                    }
                }
                manualResetEvent.WaitOne();
            } while (1 == 1);
        }

        private void determineMovementSettings()
        {
            if (directionOfDestination.Y != 0 && directionOfDestination.X != 0)      //in two directions
            {
                xIncrement = 1;
                yIncrement = Math.Abs((float)directionOfDestination.X / (float)directionOfDestination.Y); //ratio
            }
            else
            {                    //movement in ONLY one direction below
                if (directionOfDestination.Y == 0)
                {
                    yIncrement = Math.Abs(directionOfDestination.X);
                    xIncrement = 1;
                }
                else
                {
                    xIncrement = Math.Abs(directionOfDestination.Y);
                    yIncrement = 1;
                }
            }
        }

        private Point determineVectorToGo()
        {
            int demandedStepsXDirection;
            demandedStepsXDirection = Ball.CenterCoordinates.X - this.centerCoordinates.X;
            int demandedStepsYDirection;
            demandedStepsYDirection = Ball.CenterCoordinates.Y - this.centerCoordinates.Y;
            return new Point(demandedStepsXDirection, demandedStepsYDirection);
        }

        private void moveY(int direction)
        {
            if (direction < 0)
            {
                CenterCoordinatesY--;
            }
            else
            {
                CenterCoordinatesY++;
            }
        }

        private void moveX(int direction)
        {
            if (direction < 0)
            {
                CenterCoordinatesX--;
            }
            else
            {
                CenterCoordinatesX++;
            }
        }

        private bool isMovementPossible()
        {
            //foreach player in Form1.

            for (int i = 0; i < playersCoordinates.Count; i++)
            {
                if (playersCoordinates.ElementAt(i).Equals(this.CenterCoordinates))
                {
                    continue;
                }
                else
                {
                    float distanceX = playersCoordinates.ElementAt(i).X - this.CenterCoordinates.X;
                    float distanceY = playersCoordinates.ElementAt(i).Y - this.CenterCoordinates.Y;
                    double distanceBetweenPlayers = getHypotenuse(distanceX,distanceY);                        

                    if (distanceBetweenPlayers < playerCircleSize)
                    {
                        double thisBallDistance = getHypotenuse((float)this.CenterCoordinates.X - (float)Ball.CenterCoordinatesX,
                            (float)this.CenterCoordinates.Y - (float)Ball.CenterCoordinatesY);
                        double playerIBallDistance = getHypotenuse((float)playersCoordinates.ElementAt(i).X - (float)Ball.CenterCoordinates.X,
                            (float)playersCoordinates.ElementAt(i).Y - (float)Ball.CenterCoordinates.Y);

                        if (thisBallDistance > playerIBallDistance)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private double getHypotenuse(float cathetus1, float cathetus2) //from pitagoras law
        {
            return Math.Sqrt(Math.Pow(cathetus1, 2) + Math.Pow(cathetus2, 2));
        }

        public void actualizePlayerCoordiates(List<Point> playersCoordinates)
        {
            this.playersCoordinates = playersCoordinates;
        }
    }
}
