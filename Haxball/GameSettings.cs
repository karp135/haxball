﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Haxball
{
    class GameSettings
    {
        public static int leftFieldBorder = 71;
        public static int rightFieldBorder = 1078;
        public static int upperFieldBorder = 52;
        public static int bottomFieldBorder = 547;

        //private static int goalWidth =(int)(0.4 * (bottomFieldBorder - upperFieldBorder));
        public static int goalUpperPost = 205;//(int)(0.5*(bottomFieldBorder - upperFieldBorder) - 0.5 * goalWidth);
        public static int goalBottomPost = 394;//(int)(0.5*(bottomFieldBorder - upperFieldBorder) + 0.5 * goalWidth);

        public static Point centerField
        {
            get
            {
                return new Point((leftFieldBorder + rightFieldBorder) / 2, (upperFieldBorder + bottomFieldBorder) / 2);
            }
        }

        public static int playerAmount = 6; //summary amount of players on the field

    }
}
