﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Haxball
{
    public partial class Form1 : Form
    {

        int playerCircleSize = Player.playerCircleSize;

        //public static Size formSize = new Size(900, 600);

        List<Player> players1Team = new List<Player>();
        List<OvalPictureBox> playerPictureBoxes1Team = new List<OvalPictureBox>();

        List<Player> players2Team = new List<Player>();
        List<OvalPictureBox> playerPictureBoxes2Team = new List<OvalPictureBox>();

        Ball ball = new Ball();
        BallPhysics ballPhysics = new BallPhysics();
        PictureBox field = new PictureBox();

        OvalPictureBox ballPictureBox = new OvalPictureBox();

        Thread[] threads;

        public Form1()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        }

        private void Form1_Load(object sender, System.EventArgs e)
        {

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            setsPictureBoxesCoordinates();
            passPlayersCoordinatesToEachPlayer();
            setsManualResetEvents();

            Point playerAtBall = getBallCollision();
            if (new Point(0, 0) != playerAtBall)
            {
                ballPhysics.wasKicked(playerAtBall);
            }

            int goal = ballPhysics.move(); // move returns 1 if goal
            if (goal == 1)
            {
                Goal();
            }
            ballPictureBox.CircleCenterCoordinates = Ball.CenterCoordinates;

            //uncomment to test for one thread

            //playerPictureBoxes1Team.ElementAt(0).CircleCenterCoordinates = new Point(players1Team.ElementAt(0).CenterCoordinates.X,
            //         players1Team.ElementAt(0).CenterCoordinates.Y);
            //players1Team.ElementAt(0).manualResetEvent.Set();
        }

        private void setsPictureBoxesCoordinates()
        {
            for (int i = 0; i < 3; i++)
            {
                playerPictureBoxes1Team.ElementAt(i).CircleCenterCoordinates = players1Team.ElementAt(i).CenterCoordinates;
                playerPictureBoxes2Team.ElementAt(i).CircleCenterCoordinates = players2Team.ElementAt(i).CenterCoordinates;
            }
        }

        private void setsManualResetEvents()
        {
            for (int i = 0; i < 3; i++)
            {
                players1Team.ElementAt(i).manualResetEvent.Set();
                players2Team.ElementAt(i).manualResetEvent.Set();
            }
        }

        private void passPlayersCoordinatesToEachPlayer()
        {
            List<Point> playersCoordinates = new List<Point>();
            for (int i = 0; i < 3; i++)
            {
                playersCoordinates.Add(players1Team.ElementAt(i).CenterCoordinates);
            }
            for (int i = 0; i < 3; i++)
            {
                playersCoordinates.Add(players2Team.ElementAt(i).CenterCoordinates);
            }

            for (int i = 0; i < 3; i++)
            {
                players1Team.ElementAt(i).actualizePlayerCoordiates(playersCoordinates);
            }
            for (int i = 0; i < 3; i++)
            {
                players2Team.ElementAt(i).actualizePlayerCoordiates(playersCoordinates);
            }
        }

        private Point getBallCollision() //function returns coordinates of player which kick the ball
        {
            double collisionDistance = Player.playerCircleSize * 0.5 + Ball.ballCircleSize * 0.5;
            int distanceX, distanceY;
            double absoluteDistance;

            for (int i = 0; i < 3; i++)
            {
                distanceX = playerPictureBoxes1Team.ElementAt(i).CircleCenterCoordinates.X - ballPictureBox.CircleCenterCoordinates.X;
                distanceY = playerPictureBoxes1Team.ElementAt(i).CircleCenterCoordinates.Y - ballPictureBox.CircleCenterCoordinates.Y;
                absoluteDistance = Math.Sqrt(Math.Pow(distanceX, 2) + Math.Pow(distanceY, 2));

                if (absoluteDistance < collisionDistance)
                {
                    return playerPictureBoxes1Team.ElementAt(i).CircleCenterCoordinates;
                }
            }
            for (int i = 0; i < 3; i++)
            {
                distanceX = playerPictureBoxes2Team.ElementAt(i).CircleCenterCoordinates.X - ballPictureBox.CircleCenterCoordinates.X;
                distanceY = playerPictureBoxes2Team.ElementAt(i).CircleCenterCoordinates.Y - ballPictureBox.CircleCenterCoordinates.Y;
                absoluteDistance = Math.Sqrt(Math.Pow(distanceX, 2) + Math.Pow(distanceY, 2));

                if (absoluteDistance <= collisionDistance)
                {
                    return playerPictureBoxes2Team.ElementAt(i).CircleCenterCoordinates;
                }
            }
            return new Point(0, 0);
        }

        //private void drawFieldPictureBox()
        //{
        //    field.Paint += new System.Windows.Forms.PaintEventHandler(this.field_Paint);
        //    this.Controls.Add(field);
        //}

        private void drawBallPictureBox()
        {
            ballPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxBall_Paint);

            ballPictureBox.Size = new System.Drawing.Size(Ball.ballCircleSize, Ball.ballCircleSize);
            ballPictureBox.CircleCenterCoordinates = Ball.CenterCoordinates;
            this.Controls.Add(ballPictureBox);
        }

        private void draw1TeamPictureBoxes()
        {
            Random rand = new Random();

            for (int i = 0; i < GameSettings.playerAmount / 2; i++)
            {
                players1Team.Add(new Player());

                int x, y;
                do
                {
                    x = rand.Next(GameSettings.leftFieldBorder + Convert.ToInt32(0.5 * playerCircleSize),
                        GameSettings.centerField.X + 1 - Convert.ToInt32(0.5 * playerCircleSize));
                    y = rand.Next(GameSettings.upperFieldBorder + Convert.ToInt32(0.5 * playerCircleSize),
                        GameSettings.bottomFieldBorder + 1 - Convert.ToInt32(0.5 * playerCircleSize));
                }
                while (!isDrawnPlaceEmpty(players1Team, new Point(x, y)));

                players1Team.ElementAt(i).centerCoordinates = new Point(x, y);

                playerPictureBoxes1Team.Add(new OvalPictureBox());
                playerPictureBoxes1Team.ElementAt(i).Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1Team_Paint);
                playerPictureBoxes1Team.ElementAt(i).Size = new System.Drawing.Size(playerCircleSize, playerCircleSize);
                playerPictureBoxes1Team.ElementAt(i).CircleCenterCoordinates = players1Team.ElementAt(i).centerCoordinates;
                // Add the PictureBox control to the Form.
                this.Controls.Add(playerPictureBoxes1Team.ElementAt(i));
            }
        }

        private void draw2TeamPictureBoxes()
        {
            Random rand = new Random();

            for (int i = 0; i < GameSettings.playerAmount / 2; i++)
            {
                players2Team.Add(new Player());

                int x, y;
                do
                {
                    x = rand.Next(GameSettings.centerField.X + Convert.ToInt32(0.5 * playerCircleSize),
                        GameSettings.rightFieldBorder + 1 - Convert.ToInt32(0.5 * playerCircleSize));
                    y = rand.Next(GameSettings.upperFieldBorder + Convert.ToInt32(0.5 * playerCircleSize),
                        GameSettings.bottomFieldBorder + 1 - Convert.ToInt32(0.5 * playerCircleSize));
                }
                while (!isDrawnPlaceEmpty(players2Team, new Point(x, y)));

                players2Team.ElementAt(i).centerCoordinates = new Point(x, y);

                playerPictureBoxes2Team.Add(new OvalPictureBox());
                playerPictureBoxes2Team.ElementAt(i).Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2Team_Paint);
                playerPictureBoxes2Team.ElementAt(i).Size = new System.Drawing.Size(playerCircleSize, playerCircleSize);
                playerPictureBoxes2Team.ElementAt(i).CircleCenterCoordinates = players2Team.ElementAt(i).centerCoordinates;
                // Add the PictureBox control to the Form.
                this.Controls.Add(playerPictureBoxes2Team.ElementAt(i));
            }
        }

        //private void field_Paint(object sender, PaintEventArgs e)
        //{
        //    Graphics g = e.Graphics;
        //    Image image = Image.FromFile("boisko.png");
        //    g.DrawImage(image, 0, 0, this.Width, this.Height);
        //}

        // 3 painting methods below
        private void pictureBox1Team_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // Create a local version of the graphics object for the PictureBox.
            Graphics g = e.Graphics;

            // Create location and size of ellipse.
            int x = 0;
            int y = 0;
            int diameter = playerCircleSize;

            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            g.FillEllipse(myBrush, new Rectangle(x, y, diameter, diameter));
            myBrush.Dispose();
        }

        private void pictureBox2Team_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // Create a local version of the graphics object for the PictureBox.
            Graphics g = e.Graphics;

            // Create location and size of ellipse.
            int x = 0;
            int y = 0;
            int diameter = playerCircleSize;

            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Blue);
            g.FillEllipse(myBrush, new Rectangle(x, y, diameter, diameter));
            myBrush.Dispose();
        }

        private void pictureBoxBall_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // Create a local version of the graphics object for the PictureBox.
            Graphics g = e.Graphics;

            // Create location and size of ellipse.
            int x = 0;
            int y = 0;
            int diameter = Ball.ballCircleSize;

            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);
            g.FillEllipse(myBrush, new Rectangle(x, y, diameter, diameter));
            myBrush.Dispose();
            //this.BringToFront();
        }

        private bool isDrawnPlaceEmpty(List<Player> players, Point point) // method checks is drawn place empty or occupied by another player 
        {
            foreach (Player player in players)
            {
                if ((Math.Abs(player.centerCoordinates.X - point.X) > Player.playerCircleSize) &&
                    (Math.Abs(player.centerCoordinates.Y - point.Y) > Player.playerCircleSize))
                {
                    //to spoko
                }
                else
                {
                    return false;
                }
            }
            if (Math.Abs(point.X - GameSettings.centerField.X) > 0.5 * Player.playerCircleSize + 0.5 * Ball.ballCircleSize &&
                Math.Abs(point.Y - GameSettings.centerField.Y) > 0.5 * Player.playerCircleSize + 0.5 * Ball.ballCircleSize)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            stopGame();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            startGame();
            pictureBox1.SendToBack();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            stopGame();
        }

        private void startGame()
        {
            drawBallPictureBox();
            draw1TeamPictureBoxes();
            draw2TeamPictureBoxes();
            label1.Visible = false;

            threads = new Thread[6];
            for (int i = 0; i < 3; i++)
            {
                threads[i] = new Thread(players1Team.ElementAt(i).startMoving);
            }
            for (int i = 0; i < 3; i++)
            {
                threads[i + 3] = new Thread(players2Team.ElementAt(i).startMoving);
            }
            for (int i = 0; i < 6; i++)
            {
                threads[i].Start();
            }

            //comment after test
            //players1Team.ElementAt(0).CenterCoordinates = new Point(300, 350);

            //uncomment to test for one thread
            //threads[0].Start();

            timer1.Start();
        }

        public void stopGame()
        {
            foreach (Thread thread in threads)
            {
                thread.Abort();
            }
            timer1.Stop();
            ballPhysics.setBallForce(0);
            Ball.CenterCoordinates = GameSettings.centerField;
        }

        public void Goal()
        {
            label1.Visible = true;
            stopGame();
        }

    }
}
