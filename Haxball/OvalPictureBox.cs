﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Haxball
{
    class OvalPictureBox : PictureBox
    {
        public Point CircleCenterCoordinates
        {
            get
            {
                return new Point(Convert.ToInt32(this.Location.X + this.Size.Width*0.5),
                Convert.ToInt32(this.Location.Y + this.Size.Height*0.5));
            }
            set
            {
                int x = Convert.ToInt32(value.X - this.Size.Width * 0.5);
                int y = Convert.ToInt32(value.Y - this.Size.Height * 0.5);
                this.Location = new Point(x, y);
            }
        }

        public OvalPictureBox()
        {
            this.BackColor = Color.DarkGray;
        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            using (var gp = new GraphicsPath())
            {
                gp.AddEllipse(new Rectangle(0, 0, this.Width - 1, this.Height - 1));
                this.Region = new Region(gp);
            }
        }
    }
}