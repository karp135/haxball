﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;

namespace Haxball
{
    class Ball
    {
        public static int ballCircleSize = 30;

        public Point fieldBorderDirection;

        public Ball()
        {
            CenterCoordinates = GameSettings.centerField;
            fieldBorderDirection = new Point(CenterCoordinates.X, CenterCoordinates.Y); //smieci??
        }

        public static Point CenterCoordinates
        {
            get;
            set;
        }

        public static int CenterCoordinatesX
        {
            get
            {
                return CenterCoordinates.X;
            }
            set
            {
                CenterCoordinates = new Point(value, CenterCoordinates.Y);
            }
        }

        public static int CenterCoordinatesY
        {
            get
            {
                return CenterCoordinates.Y;
            }
            set
            {
                CenterCoordinates = new Point(CenterCoordinates.X, value);
            }
        }
    }
}
