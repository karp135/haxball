﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Haxball
{
    class BallPhysics
    {
        private Point vectorToGo;

        private static int speedFaster = 3;   //possible ball speeds
        private static int speedSlowlier = 1;
        private int speed = 0;

        private static int actualBallForce = 0;
        private int kickForce = 250;

        private float x, y;
        private float xIncrement, yIncrement;

        public BallPhysics()
        {
            vectorToGo = new Point();
        }

        public void wasKicked(Point player)  // p is player who kicked the ball
        {
            vectorToGo = determineVectorToGo(player);
            actualBallForce = kickForce;
            speed = speedFaster;
            determineMovementSettings();
        }

        private Point determineVectorToGo(Point player)
        {
            Point vectorToGo = new Point(Ball.CenterCoordinatesX - player.X,
                Ball.CenterCoordinatesY - player.Y);
            return vectorToGo;
        }

        private void determineMovementSettings()
        {
            x = 0;
            y = 0;

            if (vectorToGo.Y != 0 && vectorToGo.X != 0)      // ball will move in two directions
            {
                xIncrement = 1;
                yIncrement = Math.Abs((float)vectorToGo.X / (float)vectorToGo.Y); //ratio
            }
            else
            {                    //movement in ONLY one direction below
                if (vectorToGo.Y == 0)
                {
                    yIncrement = Math.Abs(vectorToGo.X);
                    xIncrement = 1;
                }
                else
                {
                    xIncrement = Math.Abs(vectorToGo.Y);
                    yIncrement = 1;
                }
            }
        }

        public int move()
        {
            if (!isInBordersX())
            {
                if (isInGoal())
                {
                    return 1; // return 1 if goal
                }
                else
                {
                    vectorToGo.X = vectorToGo.X * (-1);
                    determineMovementSettings();
                    goX(vectorToGo.X);
                }
            }

            if (!isInBordersY())
            {
                vectorToGo.Y = vectorToGo.Y * (-1);
                determineMovementSettings();
                goY(vectorToGo.Y);
            }

            if (isMoving())
            {
                if (actualBallForce < 0.7 * kickForce)
                    speed = speedSlowlier;

                if (x > y)
                {
                    goY(vectorToGo.Y);
                    y += yIncrement;
                }
                else
                {
                    goX(vectorToGo.X);
                    x += xIncrement;
                }
                actualBallForce--;
            }
            return 0;
        }

        private void goY(int direction)
        {
            if (direction < 0)
            {
                Ball.CenterCoordinatesY -= speed;
            }
            else
            {
                Ball.CenterCoordinatesY += speed;
            }
        }

        private void goX(int direction)
        {
            if (direction < 0)
            {
                Ball.CenterCoordinatesX -= speed;
            }
            else
            {
                Ball.CenterCoordinatesX += speed;
            }
        }

        public static bool isMoving()
        {
            if (actualBallForce > 0)
                return true;
            else
                return false;
        }

        private bool isInBordersX()
        {
            if (Ball.CenterCoordinatesX - 0.5 * Ball.ballCircleSize > GameSettings.leftFieldBorder &&
                Ball.CenterCoordinatesX + 0.5 * Ball.ballCircleSize < GameSettings.rightFieldBorder)
                return true;
            else
                return false;
        }

        private bool isInBordersY()
        {
            if (Ball.CenterCoordinatesY - 0.5 * Ball.ballCircleSize > GameSettings.upperFieldBorder &&
                Ball.CenterCoordinatesY + 0.5 * Ball.ballCircleSize < GameSettings.bottomFieldBorder)
                return true;
            else
                return false;
        }

        private bool isInGoal()
        {
            int goalUpperPost = GameSettings.goalUpperPost;
            int goalBottomPost = GameSettings.goalBottomPost;
            if ((Ball.CenterCoordinates.Y > goalUpperPost)
                && (Ball.CenterCoordinates.Y < goalBottomPost))
                return true;
            else
                return false;
        }

        public void setBallForce(int force)
        {
            actualBallForce = force;
        }
    }
}
